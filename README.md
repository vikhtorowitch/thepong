# README #


### Quick start

To run the game in editor open the "bootstrap.unity" scene.

### Archutecture

The game architecture is based on the Event Channel pattern (MessagingSystem). The GameManager provides game maintenance fuctions by exchanging messages with visual game components.


### Project structure (inside the Assets folder)

* Animators - *Main menu animator*
	+ Animations - *Main menu animations*

* Images - *Sprites and images*
* Sounds - *Bouncing sounds*
* Materials - *Sprites matherials*
* PhysicsMatherial - *Physics matherials for the ball and the rackets*
* Prefabs - *Game prefabs*
* Scenes - *Game scenes*
	+ bootstrap.unity - *Game systems mountpoint, startup scene with main menu*
	+ game.unity - *Gameplay scene, contains the ball, the rackets and obsctacles elements*
	+ PlayerHUD.unity - *Player controls e.g. the left-right buttons, the pause button and the scores*

* Photon - *PhotonUnityNetworking plugin (third-party) for multiplayer*
* TextMesh Pro - *TextMesh Pro plugin (third-party) for better text rendering*
* PongScripts - *Game core and other C# scripts*
	+ GameComponents - *MonoBehaviours for visual gameplay components*
		- __BallBehavior.cs__ - *Ball behaviour script*
		- __BallClientBehaviour.cs__ - *Slave ball behaviour script - used while a multiplayer session when the ball is controlled by master*
		- __BallSpawnerBehaviour.cs__ - *Script to choose the ball brefab to spawn (master/local or slave)*
		- __RacketBehaviour.cs__ - *Rackets behaviour script*
	
	+ HUD - *Scripts for the player controls buttons*
		- __HUDController.cs__ - *Main HUD controller with handlers for HUD maintenance e.g. show/hide HUD elements, update the scores etc.*
		- __HUDControls.cs__ - *One script for all HUD buttons: sends a message containing a clicked button type and state*
	
	+ Messaging system - *Event Channel pattern implementation - a subscriber subscribes to a message class, anyone can send a message*
		- Messages - *Game messages*
			+ __BallDropMessage.cs__ - *Raises by the Ball behaviour when the Ball goes outside of the board*
			+ __BallInitMessage.cs__ - *Raises by the Game Manager when needs to init the Ball*
			+ __GamePauseMessage.cs__ - *Raises by the HUD pause button or the Game Manager while multiplayer session*
			+ __InitCountDownMessage.cs__ - *Raises by the Game Manager when need to show a countdown - after the ball dropping or after starting a new game*
			+ __NetworkingMessageBase.cs__ - *Contains all messages for conversation with the networking system*
				- NetworkingMessageBase - *Abstract class - parent of all networking messages*
				- ConnectedToServerMessage - *Raises by the Multiplayer Manager when the player connected to server*
				- JoinedLobbyMessage - *Raises by the Multiplayer Manager when the player joined to lobby*
				- PlayerReadyMessage - *Raises by the Multiplayer Manager when the other player presses "Ready"*
				- StartLobbyGameMessage - *Raises by the MultiplayerManager or Game Manager when all players are ready to start*
				- BallSyncMessage - *Raises by the BallBehaviour if the client is Master and by the Multiplayer Manager if the client is Slave*
				- RacketSyncMessage - *Not used, will needs when refactoring of racket controls transmission to the slave client*
				
			+ RacketControlMessage - *Raises by racket controls when it pressed or released or by Multiplayer Manager when other player uses such controls*
			+ ReturnToMenuMessage - *Raises by HUD controller when "Return" button pressed*
		
		- __IPongMessageSubscriber.cs__ - *Interface for subscribers*
		- __MessagingSystem.cs__ - *Messaging system implementation*
		- __PongMessage.cs__ - *Abstract class - parent for all messages*
		
	+ NetworkingSystem - *All multiplayer-related stuff*
		- NetData - *Mediator classes between local messages and networking system*
			+ __NetDataBase.cs__ - *Serializable classes for network data exchange*
				- NetDataOpcode - *Constant codes to determine a data type*
				- NetDataBase - *Abstract class - parent for all network data classes*
				- PlayerReadyData - *Player Ready network message, mediator for PlayerReadyMessage*
				- StartGameData - *Start Game network message, mediator for StartLobbyGameMessage*
				- PlayerMoveData - *Player Move network message, mmediator for RacketControlMessage*
				- BallSyncData - *Ball coordinates sync data, mediator for BallSyncMessage*
				- BallDropData - *Ball drop data (when one of players scored), mediator for BallDropMMessage*
				- RacketSyncData - *Not used, mediator for RacketSyncMessage*
		
		- __IMultiplayerManager.cs__ - *Interface for Multiplayer Manager*
		- __LobbyManager.cs__ - *Simple lobby helper, process players ready state*
		- __PhotonManager.cs__ - *Quick Photon client wrapper, needs refactoring if have more time*
		
	+ UI - *User Interface related scripts*
		- __AnimatorParameterSetter.cs__ - *Allows to set animator parameters directly from UI element callbacks e.g. onClick*
		- __MenuController.cs__ - *Handler for main menu events*
	
	+ __ConfigManager.cs__ - *Helper class to work with config parameters etc.*
	+ __ConfigStorage.cs__ - *Contains Interface IConfigStorage and implementation of PlayerPrefsConfigStorage*
	+ __GameManager.cs__ - *Main game maintenance manager, singleton*
	+ __ICoroutineRunner.cs__ - *Interface for a MonoBehaviour that can run coroutines. It is GameManager for now, but this may be changed when GameManager becomes too complex.*
	+ __SceneLoader.cs__ - *Simple scene manager, could be more complex if needs to operate many scenes*
 
	
### Possible improvements

* Need to add an localization system
* Probably need to remove the Singleton implementation of the GameManager (used to easy access to the MessagingSystem and the ConfigManager)
* Refactoring of the networking system - PhotonManager looks not so nice
* The coordinates of the Ball are sent to the slave client and directly applies to the Ball transform - need to predict and interpolate ball coords between network messages
* Player controls transmitting is not ideal too - now it is directly sending signals of button down/up state, need to send coords and predict it as well as the ball coords
* Ball bouncing sounds are not transmitted to Slave

