﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameNetworkState
{
    Local = 0,
    Master = 1,
    Client = 2
}

public class GameManager : MonoBehaviour, ICoroutineRunner, 
    IPongMessageSubscriber<BallDropMessage>,
    IPongMessageSubscriber<BallSyncMessage>, 
    IPongMessageSubscriber<StartLobbyGameMessage>, 
    IPongMessageSubscriber<ConnectedToServerMessage>,
    IPongMessageSubscriber<RacketControlMessage>,
    IPongMessageSubscriber<ReturnToMenuMessage>
{
    public SceneLoader SceneLoader;
    
    // Simple singleton immplementation
    public static GameManager Instance { get; private set; }

    public ConfigManager ConfigManager { get; private set; }
    
    public IMessagingSystem MessagingSystem { get; private set; }

    public List<Sprite> BallSkins;

    public PhotonManager MultiplayerManager;

    public static GameNetworkState NetworkState { get; private set; }
    public static int LocalPlayerId;
    
    private LobbyManager _lobbyManager;

    private string _lobbyName;
    
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
        
        Init();
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        GameManager.Instance.MessagingSystem.SendMessage(new GamePauseMessage(true));
    }

    private void Init()
    {
        NetworkState = GameNetworkState.Local;
        
        MultiplayerManager.Init();
        
        ConfigManager = new ConfigManager();
        MessagingSystem = new MessagingSystem(this);
        
        _lobbyManager = new LobbyManager(MultiplayerManager, MessagingSystem);

        MessagingSystem.SubscribeTo<IPongMessageSubscriber<BallDropMessage>, BallDropMessage>(this);
        MessagingSystem.SubscribeTo<IPongMessageSubscriber<BallSyncMessage>, BallSyncMessage>(this);
        MessagingSystem.SubscribeTo<IPongMessageSubscriber<ConnectedToServerMessage>, ConnectedToServerMessage>(this);
        MessagingSystem.SubscribeTo<IPongMessageSubscriber<StartLobbyGameMessage>, StartLobbyGameMessage>(this);
        MessagingSystem.SubscribeTo<IPongMessageSubscriber<RacketControlMessage>, RacketControlMessage>(this);
        MessagingSystem.SubscribeTo<IPongMessageSubscriber<ReturnToMenuMessage>, ReturnToMenuMessage>(this);
    }

    public void StartLocalGame()
    {
        NetworkState = GameNetworkState.Local;
        SceneLoader.LoadScenes();
    }

    public void StartNetworkGame(string lobbyName)
    {
        LocalPlayerId = 0;

        ConfigManager.PrimaryPlayerNetScore = 0;
        
        NetworkState = GameNetworkState.Master;
        _lobbyManager.SetMaster(true);
        
        MultiplayerManager.Connect();
        _lobbyName = lobbyName;
    }

    public void JoinNetworkGame(string lobbyName)
    {
        LocalPlayerId = 1;
        
        ConfigManager.SecondaryPlayerNetScore = 0;
        
        NetworkState = GameNetworkState.Client;
        _lobbyManager.SetMaster(false);
        
        MultiplayerManager.Connect();
        _lobbyName = lobbyName;
    }

    public void PlayerReady()
    {
        _lobbyManager.SetLocalPlayerReady(true);
    }
    
    public void StopNetworkGame()
    {
        MultiplayerManager.Disconnect();
    }

    public string GetScoreString()
    {
        if (NetworkState == GameNetworkState.Local)
        {
            return $"{ConfigManager.PrimaryPlayerWins} : {ConfigManager.SecondaryPlayerWins}";
        }
        else
        {
            {
                return $"{ConfigManager.PrimaryPlayerNetScore} : {ConfigManager.SecondaryPlayerNetScore}";
            }
        }
    }
    
    public void OnMessageReceived(BallDropMessage messageData)
    {
        if (messageData.IsPrimaryVoid)
        {
            if (NetworkState != GameNetworkState.Local)
            {
                ConfigManager.SecondaryPlayerNetScore++;
            }
            else
            {
                ConfigManager.SecondaryPlayerWins++;
            }
        }
        else
        {
            if (NetworkState != GameNetworkState.Local)
            {
                ConfigManager.PrimaryPlayerNetScore++;
            }
            else
            {
                ConfigManager.PrimaryPlayerWins++;
            }
        }
        
        if (NetworkState == GameNetworkState.Master)
        {
            MultiplayerManager.SendData(NetDataOpcode.BallDrop, new BallDropData(messageData.IsPrimaryVoid ? 1 : 0));
        }
        
        GameManager.Instance.MessagingSystem.SendMessage(new InitCountdownMessage());
    }

    public void OnMessageReceived(RacketControlMessage messageData)
    {
        if (messageData.PlayerId == 0)
        {
            PlayerMoveData moveData = new PlayerMoveData(messageData);
            MultiplayerManager.SendData((byte)moveData.Opcode, moveData);
        }
    }

    public void OnMessageReceived(BallSyncMessage messageData)
    {
        if (NetworkState != GameNetworkState.Master)
        {
            return;
        }
        
        BallSyncData syncData = new BallSyncData(messageData);
        
        MultiplayerManager.SendData((byte)syncData.Opcode, syncData);
    }

    public void OnMessageReceived(StartLobbyGameMessage messageData)
    {
        SceneLoader.LoadScenes();
        GameManager.Instance.MessagingSystem.SendMessage(new InitCountdownMessage());
    }

    public void OnMessageReceived(ConnectedToServerMessage messageData)
    {
        if (NetworkState == GameNetworkState.Master)
        {
            MultiplayerManager.CreateLobby(_lobbyName);
        }
        else
        {
            MultiplayerManager.JoinLobby(_lobbyName);
        }
    }

    public void OnMessageReceived(ReturnToMenuMessage messageData)
    {
        MultiplayerManager.Disconnect();
        SceneLoader.UnloadScenes();
    }
}