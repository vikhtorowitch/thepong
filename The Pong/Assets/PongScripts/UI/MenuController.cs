﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class MenuController : MonoBehaviour, 
    IPongMessageSubscriber<ConnectedToServerMessage>, 
    IPongMessageSubscriber<JoinedLobbyMessage>, 
    IPongMessageSubscriber<StartLobbyGameMessage>,
    IPongMessageSubscriber<ReturnToMenuMessage>
{
    public Animator MenuAnimator;

    public TMP_Dropdown BallSkinsDropdown;
    public TMP_Text LobbyStatusText;
    public TMP_InputField LobbyNameInput;
    public Toggle BallChaosModeToggle;

    private void Start()
    {
        GameManager.Instance.MessagingSystem.SubscribeTo<IPongMessageSubscriber<ConnectedToServerMessage>, ConnectedToServerMessage>(this);
        GameManager.Instance.MessagingSystem.SubscribeTo<IPongMessageSubscriber<JoinedLobbyMessage>, JoinedLobbyMessage>(this);
        GameManager.Instance.MessagingSystem.SubscribeTo<IPongMessageSubscriber<StartLobbyGameMessage>, StartLobbyGameMessage>(this);
        GameManager.Instance.MessagingSystem.SubscribeTo<IPongMessageSubscriber<ReturnToMenuMessage>, ReturnToMenuMessage>(this);
        
        Init();
    }
    
    public void Init()
    {
        Assert.IsNotNull(MenuAnimator);
        Assert.IsNotNull(BallSkinsDropdown);
        
        UpdateBallSkinsDropdown();

        BallSkinsDropdown.value = GameManager.Instance.ConfigManager.BallSkinIndex;
        BallChaosModeToggle.isOn = GameManager.Instance.ConfigManager.BallChaosMode;
    }

    public void OnStartLocalGame()
    {
        MenuAnimator.SetBool("Show", false);
        GameManager.Instance.StartLocalGame();
    }
    
    private void UpdateBallSkinsDropdown()
    {
        BallSkinsDropdown.options.Clear();

        foreach (var skin in GameManager.Instance.BallSkins)
        {
            var option = new TMP_Dropdown.OptionData {image = skin};
            BallSkinsDropdown.options.Add(option);
        }
    }

    public void CreateLobby()
    {
        GameManager.Instance.StartNetworkGame(LobbyNameInput.text);
    }

    public void JoinLobby()
    {
        GameManager.Instance.JoinNetworkGame(LobbyNameInput.text);
    }

    public void PlayerReady()
    {
        GameManager.Instance.PlayerReady();
    }

    public void CancelLobby()
    {
        GameManager.Instance.StopNetworkGame();
    }
    
    public void OnBallSkinsDropdownChanged()
    {
        GameManager.Instance.ConfigManager.BallSkinIndex = BallSkinsDropdown.value;
    }

    public void ResetScore()
    {
        GameManager.Instance.ConfigManager.PrimaryPlayerWins = 0;
        GameManager.Instance.ConfigManager.SecondaryPlayerWins = 0;
    }

    public void OnBallChaosToggleValueChanged(bool isOn)
    {
        GameManager.Instance.ConfigManager.BallChaosMode = isOn;
    }
    
    public void OnMessageReceived(ConnectedToServerMessage messageData)
    {
        Debug.Log("[MenuController] Connected received");
        
        if (!messageData.Success)
        {
            LobbyStatusText.text = "Error: " + messageData.Message;
        }
    }

    public void OnMessageReceived(JoinedLobbyMessage messageData)
    {
        Debug.Log("[MenuController] JoinedLobby received");
        
        if (messageData.Success)
        {
            if (GameManager.NetworkState == GameNetworkState.Master)
            {
                LobbyStatusText.text = "Lobby created! Waiting for other player connected and ready!";
            }
            else
            {
                LobbyStatusText.text = "Joined lobby! Waiting for other player ready!";
            }
        }
        else
        {
            LobbyStatusText.text = "Error: " + messageData.Message;
        }
    }

    public void OnMessageReceived(StartLobbyGameMessage messageData)
    {
        MenuAnimator.SetBool("Show", false);
    }

    public void OnMessageReceived(ReturnToMenuMessage messageData)
    {
        MenuAnimator.SetBool("Show", true);
    }
}
