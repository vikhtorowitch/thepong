﻿using UnityEngine;

public class AnimatorParameterSetter : MonoBehaviour
{
    public Animator Animator;
    
    public void SetTrigger(string name)
    {
        Animator.SetTrigger(name);
    }
}