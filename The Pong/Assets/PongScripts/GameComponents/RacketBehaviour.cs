﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacketBehaviour : MonoBehaviour, IPongMessageSubscriber<RacketControlMessage>
{
    public int PlayerId;
    
    private Rigidbody2D _rigidbody;
    private Collider2D _collider;

    private Guid _racketControlMessage;

    private ControlButtonType _lastButtonDown;
    
    float _velocityX = 0;
    
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
    }

    private void Start()
    {
        _racketControlMessage = GameManager.Instance.MessagingSystem.SubscribeTo<IPongMessageSubscriber<RacketControlMessage>, RacketControlMessage>(this);
    }
    
    public void OnMessageReceived(RacketControlMessage messageData)
    {
        if (messageData.PlayerId == PlayerId)
        {
            if (messageData.ButtonType == ControlButtonType.Left)
            {
                if (messageData.IsButtonPressed)
                {
                    _velocityX = -10f;
                    _lastButtonDown = messageData.ButtonType;
                }
                else
                {
                    if (_lastButtonDown == ControlButtonType.Left)
                    {
                        _velocityX = 0f;
                    }
                }
            }
            else if (messageData.ButtonType == ControlButtonType.Right)
            {
                if (messageData.IsButtonPressed)
                {
                    _velocityX = 10f;
                    _lastButtonDown = messageData.ButtonType;
                }
                else
                {
                    if (_lastButtonDown == ControlButtonType.Right)
                    {
                        _velocityX = 0f;
                    }
                }
            }

            _rigidbody.velocity = new Vector2(_velocityX, 0f);
        }
    }

    private void Update()
    {
        
        if (Mathf.Abs(transform.localPosition.x) > 6.5f)
        {
            if (transform.localPosition.x < 0)
            {
                _rigidbody.velocity = Vector2.right;
            }
            else
            {
                _rigidbody.velocity = Vector2.left;
            }
        }
    }

    private void OnDestroy()
    {
        GameManager.Instance.MessagingSystem.Unsubscribe<RacketControlMessage>(_racketControlMessage);
    }
}
