﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawnerBehaviour : MonoBehaviour
{
    public AudioSource[] SoundsList;
    
    public GameObject BallMasterPrefab;
    public GameObject BallClientPrefab;

    public Transform ArenaRootTransform;

    private void Start()
    {
        SpawnBall();
    }

    public void SpawnBall()
    {
        // TODO: Make a prefab manager and a factory
        GameObject ballInstance;
        
        if (GameManager.NetworkState == GameNetworkState.Client)
        {
            ballInstance = Instantiate(BallClientPrefab);
            ballInstance.transform.SetParent(ArenaRootTransform);
        }
        else
        {
            ballInstance = Instantiate(BallMasterPrefab);
            
            var ball = ballInstance.GetComponent<BallBehaviour>();
            ball.SetSounds(SoundsList);
        }
        
        ballInstance.transform.SetParent(ArenaRootTransform);
    }
}
