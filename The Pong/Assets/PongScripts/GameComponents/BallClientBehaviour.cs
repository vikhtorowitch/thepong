﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine;

public class BallClientBehaviour : MonoBehaviour, IPongMessageSubscriber<BallInitMessage>, IPongMessageSubscriber<BallSyncMessage>
{
    public AudioSource PrimaryRacketSound;
    public AudioSource SecondaryRacketSound;
    public AudioSource WallSound;
    
    private SpriteRenderer _spriteRenderer;

    private Vector2 _savedVelocity;

    private Guid _ballInitMessageGuid;
    private Guid _ballSyncMessageGuid;
   
    public void Init()
    {
        gameObject.SetActive(true);
        
        int spriteIndex = GameManager.Instance.ConfigManager.BallSkinIndex;
        _spriteRenderer.sprite = GameManager.Instance.BallSkins[spriteIndex];

        transform.localPosition = Vector3.zero;
    }

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        Assert.IsNotNull(_spriteRenderer);

        _ballInitMessageGuid = GameManager.Instance.MessagingSystem.SubscribeTo<IPongMessageSubscriber<BallInitMessage>, BallInitMessage>(this);
        _ballSyncMessageGuid = GameManager.Instance.MessagingSystem.SubscribeTo<IPongMessageSubscriber<BallSyncMessage>, BallSyncMessage>(this);
        
        gameObject.SetActive(false);
    }

    public void OnMessageReceived(BallInitMessage messageData)
    {
        Init();
    }

    public void OnMessageReceived(BallSyncMessage messageData)
    {
        if (GameManager.NetworkState == GameNetworkState.Client)
        {
            messageData.LocalPosition.y *= -1;
            messageData.LocalEuler.x += 180;
            
            transform.localPosition = messageData.LocalPosition;
            transform.localRotation = Quaternion.Euler(messageData.LocalEuler);
        }
    }

    private void OnDestroy()
    {
        GameManager.Instance.MessagingSystem.Unsubscribe<BallInitMessage>(_ballInitMessageGuid);
        GameManager.Instance.MessagingSystem.Unsubscribe<BallSyncMessage>(_ballSyncMessageGuid);
    }
}
