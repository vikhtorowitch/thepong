﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine;
using Random = UnityEngine.Random;

public class BallBehaviour : MonoBehaviour, IPongMessageSubscriber<BallInitMessage>, IPongMessageSubscriber<GamePauseMessage>
{
    public AudioSource PrimaryRacketSound;
    public AudioSource SecondaryRacketSound;
    public AudioSource WallSound;
    
    private Rigidbody2D _rigidbody;
    private Collider2D _collider;
    private SpriteRenderer _spriteRenderer;

    private Vector2 _savedVelocity;

    private Guid _ballInitMessage;
    private Guid _gamePauseMessage;
    
    public void Init()
    {
        gameObject.SetActive(true);
        
        int spriteIndex = GameManager.Instance.ConfigManager.BallSkinIndex;
        _spriteRenderer.sprite = GameManager.Instance.BallSkins[spriteIndex];

        int forceMultiplier = 400;

        if (GameManager.NetworkState == GameNetworkState.Local && GameManager.Instance.ConfigManager.BallChaosMode)
        {
            float rndScale = Random.Range(0.7f, 1.3f);
            Color rndColor = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);

            _spriteRenderer.color = rndColor;
            gameObject.transform.localScale = new Vector3(rndScale, rndScale, 1);

            forceMultiplier = Random.Range(200, 700);
        }

        _rigidbody.WakeUp();
      
        _rigidbody.velocity = Vector2.zero;
        transform.localPosition = Vector3.zero;

        float randX = Random.value - 0.5f;
        float randY = Random.value - 0.5f;
    
        _rigidbody.AddForce(new Vector2(randX, randY).normalized * forceMultiplier, ForceMode2D.Force);
    }

    public void SetSounds(AudioSource[] soundsList)
    {
        PrimaryRacketSound = soundsList[0];
        SecondaryRacketSound = soundsList[1];
        WallSound = soundsList[2];
    }
    
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        Assert.IsNotNull(_rigidbody);
        Assert.IsNotNull(_collider);
        Assert.IsNotNull(_spriteRenderer);

        _ballInitMessage = GameManager.Instance.MessagingSystem.SubscribeTo<IPongMessageSubscriber<BallInitMessage>, BallInitMessage>(this);
        _gamePauseMessage = GameManager.Instance.MessagingSystem.SubscribeTo<IPongMessageSubscriber<GamePauseMessage>, GamePauseMessage>(this);
        
        gameObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        if (GameManager.NetworkState == GameNetworkState.Master)
        {
            GameManager.Instance.MessagingSystem.SendMessage(new BallSyncMessage(transform.localPosition, transform.localEulerAngles));
        }
    }
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        // TODO: Make a conventional sound system based on audio sources pool
        if (other.gameObject.CompareTag("PrimaryRacket"))
        {
            PrimaryRacketSound.Play();
        } 
        else if (other.gameObject.CompareTag("SecondaryRacket"))
        {
            SecondaryRacketSound.Play();
        }
        else if (other.gameObject.CompareTag("Obstacle"))
        {
            WallSound.Play();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("PrimaryVoid"))
        {
            GameManager.Instance.MessagingSystem.SendMessage(new BallDropMessage(true));
        }
        
        if (other.CompareTag("SecondaryVoid"))
        {
            GameManager.Instance.MessagingSystem.SendMessage(new BallDropMessage(false));
        }

        _rigidbody.Sleep();
    }

    public void OnMessageReceived(BallInitMessage messageData)
    {
        Init();
    }

    public void OnMessageReceived(GamePauseMessage messageData)
    {
        if (messageData.IsPaused)
        {
            _savedVelocity = _rigidbody.velocity;
            _rigidbody.Sleep();
        }
        else
        {
            _rigidbody.WakeUp();
            _rigidbody.velocity = _savedVelocity;
        }
    }

    private void OnDestroy()
    {
        GameManager.Instance.MessagingSystem.Unsubscribe<BallInitMessage>(_ballInitMessage);
        GameManager.Instance.MessagingSystem.Unsubscribe<GamePauseMessage>(_gamePauseMessage);
    }
}
