﻿
public class ConfigManager
{
    public IConfigStorage ConfigStorage;

    public int StartCountdown = 3;

    public int PrimaryPlayerNetScore;
    public int SecondaryPlayerNetScore;
    
    public const string BallSkinIndexKey = "kBallSkinIndex";
    public int BallSkinIndex
    {
        set
        {
            ConfigStorage.SetInt(BallSkinIndexKey, value);
        }
        get
        {
           return ConfigStorage.GetInt(BallSkinIndexKey, 0);
        }
    }

    public const string PrimaryPlayerWinsKey = "kPrimaryPlayerWins";
    public int  PrimaryPlayerWins
    {
        set
        {
            ConfigStorage.SetInt(PrimaryPlayerWinsKey, value);
        }
        get
        {
            return ConfigStorage.GetInt(PrimaryPlayerWinsKey, 0);
        }
    }

    public const string SecondaryPlayerWinsKey = "kPrimarySecondaryWins";
    public int SecondaryPlayerWins
    {
        set
        {
            ConfigStorage.SetInt(SecondaryPlayerWinsKey, value);
        }
        get
        {
            return ConfigStorage.GetInt(SecondaryPlayerWinsKey, 0);
        }
    }

    public const string ShowTipsKey = "kShowTips";
    public bool ShowTips
    {
        set
        {
            ConfigStorage.SetBool(ShowTipsKey, value);
        }
        get
        {
            return ConfigStorage.GetBool(ShowTipsKey, true);
        }
    }

    public const string BallChaosModeKey = "kBallChaosMode";
    public bool BallChaosMode
    {
        set
        {
            ConfigStorage.SetBool(BallChaosModeKey, value);
        }
        get
        {
            return ConfigStorage.GetBool(BallChaosModeKey, false);
        }
    }
    
    
    public ConfigManager()
    {
        ConfigStorage = new PlayerPrefsConfigStorage();
    }
}

