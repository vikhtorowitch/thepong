﻿public interface IPongMessageSubscriber<T>
{
    void OnMessageReceived(T messageData);
}

public abstract class PongMessageSubscriber<T>
{
    public abstract void OnMessageReceived(T messageData);
}