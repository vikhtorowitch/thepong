﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public interface IMessagingSystem
{
    Guid SubscribeTo<T1, T2>(T1 subscriber)        
        where T1 : IPongMessageSubscriber<T2> 
        where T2 : PongMessage;
    void SendMessage(PongMessage message);
    void Unsubscribe<T>(Guid guid) where T : PongMessage;
}

public class MessagingSystem : IMessagingSystem
{
    private interface ISubscriberHandler
    {
        void Call(PongMessage messageData);
    }
    
    private class SubscriberHandler<T, TT> : ISubscriberHandler where T : PongMessage where TT : class, IPongMessageSubscriber<T>
    {
        private TT _subscriber;

        public SubscriberHandler(TT subscriber)
        {
            _subscriber = subscriber;
        }
        
        public void Call(PongMessage messageData)
        {
            _subscriber.OnMessageReceived((T)messageData);
        }
    }
    
    private ICoroutineRunner _coroutineRunner;

    private Coroutine _workingRoutine = null;

    private Dictionary<Type, Dictionary<Guid, ISubscriberHandler>> _subscribers;

    private Queue<PongMessage> _messagesQueue;

    private object _dictionaryLockObject = new object();
    private object _queueLockObject = new object();
    
    public MessagingSystem(ICoroutineRunner coroutineRunner)
    {
        Assert.IsNotNull(coroutineRunner);
        _coroutineRunner = coroutineRunner;
        _subscribers = new Dictionary<Type, Dictionary<Guid, ISubscriberHandler>>();
        _messagesQueue = new Queue<PongMessage>();
    }

    public Guid SubscribeTo<T, TT>(T subscriber) 
        where T : IPongMessageSubscriber<TT> 
        where TT : PongMessage
    {
        Type type = typeof(TT);
        
        // The guid is needed if we want to find a subscriber for example to unsubscribe.
        Guid guid = Guid.NewGuid();
        
        Dictionary<Guid, ISubscriberHandler> subList = null;

        lock (_dictionaryLockObject)
        {
            if (_subscribers.ContainsKey(type))
            {
                subList = _subscribers[type];
            }
            else
            {
                subList = new Dictionary<Guid, ISubscriberHandler>();
                _subscribers.Add(type, subList);
            }

            ISubscriberHandler subHandler = new SubscriberHandler<TT, IPongMessageSubscriber<TT>>(subscriber);
            
            subList.Add(guid, subHandler);
        }

        return guid;
    }

    public void Unsubscribe<T>(Guid guid) where T : PongMessage
    {
        Type type = typeof(T);
        
        lock (_dictionaryLockObject)
        {
            if (_subscribers.ContainsKey(type))
            {
                var subList = _subscribers[type];

                if (subList.ContainsKey(guid))
                {
                    subList.Remove(guid);
                }
            }
        }
    }

    public void SendMessage(PongMessage message)
    {
        lock (_queueLockObject)
        {
            _messagesQueue.Enqueue(message);
        }

        LazyRunCoroutine();
    }
    
    private void LazyRunCoroutine()
    {
        if (_workingRoutine != null)
        {
            return;
        }

        _workingRoutine = _coroutineRunner.StartCoroutine(WorkingRoutine());
    }

    private IEnumerator WorkingRoutine()
    {
        while (true)
        {
            if (_messagesQueue.Count > 0)
            {
                PongMessage message;
                lock (_queueLockObject)
                {
                    message = _messagesQueue.Dequeue();
                }

                CallSubscribers(message);
            }

            yield return null;
        }

        yield break;
    }

    private void CallSubscribers(PongMessage message)
    {
        Type type = message.GetType();

        lock (_dictionaryLockObject)
        {
            if (_subscribers.ContainsKey(type))
            {
                var subList = _subscribers[type];
                foreach (var item in subList)
                {
                    item.Value.Call(message);
                }
            }
        }
    }
}