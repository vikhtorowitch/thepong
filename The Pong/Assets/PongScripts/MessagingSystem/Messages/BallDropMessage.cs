﻿public class BallDropMessage : PongMessage
{
    public bool IsPrimaryVoid;

    public BallDropMessage(bool isPrimaryVoid)
    {
        IsPrimaryVoid = isPrimaryVoid;
    }
}
