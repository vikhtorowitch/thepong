﻿public enum ControlButtonType
{
    Left = 0,
    Right = 1,
    Pause = 2,
    Tips = 3
}

public class RacketControlMessage : PongMessage
{
    public int PlayerId;
    public ControlButtonType ButtonType;
    public bool IsButtonPressed;

    public RacketControlMessage(int playerId, ControlButtonType buttonType, bool isButtonPressed)
    {
        PlayerId = playerId;
        ButtonType = buttonType;
        IsButtonPressed = isButtonPressed;
    }

    public RacketControlMessage(PlayerMoveData moveData)
    {
        PlayerId = 1;
        ButtonType = moveData.ButtonType;
        IsButtonPressed = moveData.IsButtonPressed;
    }
}