﻿
public class GamePauseMessage : PongMessage
{
    public bool IsPaused;

    public GamePauseMessage(bool isPaused)
    {
        IsPaused = isPaused;
    }
}