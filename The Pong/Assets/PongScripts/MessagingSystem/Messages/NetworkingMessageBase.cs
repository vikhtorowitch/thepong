﻿
using UnityEngine;

public abstract class NetworkingMessageBase : PongMessage
{
    public bool Success;
    public string Message;

    public NetworkingMessageBase(bool success, string message = null)
    {
        Success = success;
        Message = message;
    }
}

public class ConnectedToServerMessage : NetworkingMessageBase
{
    public ConnectedToServerMessage(bool success, string message = null) : base(success, message)
    {
    }
}

public class JoinedLobbyMessage : NetworkingMessageBase
{
    public JoinedLobbyMessage(bool success, string message = null) : base(success, message)
    {
    }
}


public class PlayerReadyMessage : NetworkingMessageBase
{
    public PlayerReadyMessage(bool success, string message = null) : base(success, message)
    {
    }
}

public class StartLobbyGameMessage : NetworkingMessageBase
{
    public StartLobbyGameMessage(bool success, string message = null) : base(success, message)
    {
    }
}

public class BallSyncMessage : PongMessage
{
    public Vector3 LocalPosition;
    public Vector3 LocalEuler;
    
    public BallSyncMessage(Vector3 localPosition, Vector3 localEuler)
    {
        LocalPosition = localPosition;
        LocalEuler = localEuler;
    }

    public BallSyncMessage(BallSyncData syncData)
    {
        LocalPosition = syncData.LocalPosition;
        LocalEuler = syncData.LocalEuler;
    }
}

public class RacketSyncMessage : PongMessage
{
    public int PlayerId;
    public Vector3 LocalPosition;

    public RacketSyncMessage(int playerId, Vector3 localPosition)
    {
        PlayerId = playerId;
        LocalPosition = localPosition;
    }
}