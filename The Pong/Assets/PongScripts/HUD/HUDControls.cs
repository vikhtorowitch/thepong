﻿using UnityEngine;
using UnityEngine.EventSystems;

public class HUDControls : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public int PlayerId;
    public ControlButtonType ButtonType;

    private void Start()
    {
        if (GameManager.NetworkState != GameNetworkState.Local && PlayerId == 1)
        {
            gameObject.SetActive(false);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        GameManager.Instance.MessagingSystem.SendMessage(new RacketControlMessage(PlayerId, ButtonType, true));
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        GameManager.Instance.MessagingSystem.SendMessage(new RacketControlMessage(PlayerId, ButtonType, false));
    }
}