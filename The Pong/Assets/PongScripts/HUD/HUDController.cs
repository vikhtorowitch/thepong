﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class HUDController : MonoBehaviour, IPongMessageSubscriber<RacketControlMessage>, IPongMessageSubscriber<InitCountdownMessage>
{
    public GameObject StartGameContainer;
    public GameObject PauseGameContainer;
    public GameObject FTUEContainer;

    public TMP_Text CoundownLabel;

    public TMP_Text ScoreText;

    private int _countDownInitValue;
    private bool _paused = false;

    private Guid _racketControlMessageGuid;
    private Guid _initCountdownMessageGuid;
    
    private void Start()
    {
        _racketControlMessageGuid = GameManager.Instance.MessagingSystem.SubscribeTo<IPongMessageSubscriber<RacketControlMessage>, RacketControlMessage>(this);
        _initCountdownMessageGuid = GameManager.Instance.MessagingSystem.SubscribeTo<IPongMessageSubscriber<InitCountdownMessage>, InitCountdownMessage>(this);
        
        _countDownInitValue = GameManager.Instance.ConfigManager.StartCountdown;
        CoundownLabel.text = _countDownInitValue.ToString();
        
        InitCountdown();
    }

    public void InitCountdown()
    {
        ScoreText.text = GameManager.Instance.GetScoreString();
        
        PauseGameContainer.SetActive(false);
        
        if (GameManager.Instance.ConfigManager.ShowTips)
        {
            ContinueInitWithFTUE();
        }
        else
        {
            ContinueInitWithCountdown();
        }
    }

    private void ContinueInitWithFTUE()
    {
        StartGameContainer.SetActive(false);
        FTUEContainer.SetActive(true);

        GameManager.Instance.ConfigManager.ShowTips = false;
    }
    
    private void ContinueInitWithCountdown()
    {
        StartGameContainer.SetActive(true);
        FTUEContainer.SetActive(false);
        PauseGameContainer.SetActive(false);
            
        StartCoroutine(StartCountdownCoroutine());
    }
    
    public void OnStartButton()
    {
        StartGameContainer.SetActive(false);
        PauseGameContainer.SetActive(false);
        FTUEContainer.SetActive(false);
        GameManager.Instance.MessagingSystem.SendMessage(new BallInitMessage());
    }
    
    public void OnPauseButton()
    {
        _paused = !_paused;
        
        PauseGameContainer.SetActive(_paused);
        
        GameManager.Instance.MessagingSystem.SendMessage(new GamePauseMessage(_paused));
    }

    private IEnumerator StartCountdownCoroutine()
    {
        int count = _countDownInitValue;

        while (count > 0)
        {
            CoundownLabel.text = count.ToString();
            
            yield return new WaitForSeconds(1f);
            count--;
        }
        
        OnStartButton();
    }

    public void OnReturnToMenuClick()
    {
        GameManager.Instance.MessagingSystem.SendMessage(new ReturnToMenuMessage());
    }
    
    public void OnMessageReceived(RacketControlMessage messageData)
    {
        if (messageData.ButtonType == ControlButtonType.Pause && messageData.IsButtonPressed)
        {
            OnPauseButton();
        }
        
        if (messageData.ButtonType == ControlButtonType.Tips && messageData.IsButtonPressed)
        {
            ContinueInitWithCountdown();
        }

    }

    public void OnMessageReceived(InitCountdownMessage messageData)
    {
        InitCountdown();
    }

    private void OnDestroy()
    { 
        GameManager.Instance.MessagingSystem.Unsubscribe<RacketControlMessage>(_racketControlMessageGuid);
        GameManager.Instance.MessagingSystem.Unsubscribe<InitCountdownMessage>(_initCountdownMessageGuid);
    }
}