﻿
using UnityEngine;

public class LobbyManager : IPongMessageSubscriber<PlayerReadyMessage>
{
    private IMultiplayerManager _multiplayerManager;
    private IMessagingSystem _messagingSystem;

    private bool _isLocalPlayerReady;
    private bool _isOtherPlayerReady;

    private bool _isMaster = false;
    
    public LobbyManager(IMultiplayerManager multiplayerManager, IMessagingSystem messagingSystem)
    {
        _multiplayerManager = multiplayerManager;
        _messagingSystem = messagingSystem;
        
        _messagingSystem.SubscribeTo<IPongMessageSubscriber<PlayerReadyMessage>, PlayerReadyMessage>(this);
    }

    public void SetMaster(bool isMaster)
    {
        _isMaster = isMaster;
    }
    
    public void CheckReadyAndStart()
    {
        if (_isMaster && _isLocalPlayerReady && _isOtherPlayerReady)
        {
            StartGameData gameData = new StartGameData();
            _multiplayerManager.SendData((byte)gameData.Opcode, gameData);
            _messagingSystem.SendMessage(new StartLobbyGameMessage(true));
        }
    }
    
    public void SetLocalPlayerReady(bool isReady)
    {
        _isLocalPlayerReady = isReady;

        if (!_isMaster)
        {
            PlayerReadyData readyData = new PlayerReadyData(isReady);
            _multiplayerManager.SendData((byte) readyData.Opcode, readyData);
        }
        else
        {
            CheckReadyAndStart();
        }
    }
    
    public void OnMessageReceived(PlayerReadyMessage messageData)
    {
        Debug.Log("[LobbyManager] Other player ready received");
        
        _isOtherPlayerReady = messageData.Success;

        if (_isMaster)
        {
            CheckReadyAndStart();
        }
    }
}
