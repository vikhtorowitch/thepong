﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class PhotonManager : MonoBehaviourPunCallbacks, IMultiplayerManager, IOnEventCallback
{
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void Init()
    {
        PhotonNetwork.AddCallbackTarget(this);
        PhotonNetwork.AutomaticallySyncScene = false;
        PhotonNetwork.GameVersion = "1";
    }

    public void Connect()
    {
        Debug.Log("[Photon] Connecting...");
        
        PhotonNetwork.OfflineMode = false;

        PhotonNetwork.LocalPlayer.NickName = Guid.NewGuid().ToString();
        PhotonNetwork.ConnectUsingSettings();
    }

    public void CreateLobby(string lobbyName)
    {
        Debug.Log("[Photon] Create room sent...");
        PhotonNetwork.JoinOrCreateRoom(lobbyName, new RoomOptions() { MaxPlayers = 2 }, null);
    }

    public void JoinLobby(string lobbyName)
    {
        Debug.Log("[Photon] Join random room sent...");
        
        PhotonNetwork.JoinRoom(lobbyName);
    }

    public void Disconnect()
    {
        Debug.Log("[Photon] Disconnect...");
        PhotonNetwork.Disconnect();
    }
    
    public void SendData(byte opcode, NetDataBase sendData)
    {
        if (!PhotonNetwork.InRoom)
        {
            return;
        }
        
        PhotonNetwork.RaiseEvent(opcode, sendData.ToObjArray(), new RaiseEventOptions() { Receivers = ReceiverGroup.Others}, SendOptions.SendReliable);
    }
   
    // Just time economy, of course here should be inner events to subscribe throught interface

    public void OnEvent(EventData photonEvent)
    {
        switch (photonEvent.Code)
        {
            case NetDataOpcode.PlayerReady:
                GameManager.Instance.MessagingSystem.SendMessage(new PlayerReadyMessage(true));
                break;
            
            case NetDataOpcode.StartGame:
                GameManager.Instance.MessagingSystem.SendMessage(new StartLobbyGameMessage(true));
                break;
            
            case NetDataOpcode.PlayerMove:
                PlayerMoveData moveData = new PlayerMoveData();
                moveData.FromObjArray(photonEvent.CustomData);
                
                RacketControlMessage controlMessage = new RacketControlMessage(moveData);

                GameManager.Instance.MessagingSystem.SendMessage(controlMessage);
                break;
            
            case NetDataOpcode.BallSync:
                BallSyncData syncData = new BallSyncData();
                syncData.FromObjArray((object[])photonEvent.CustomData);
                BallSyncMessage syncMessage = new BallSyncMessage(syncData);

                GameManager.Instance.MessagingSystem.SendMessage(syncMessage);
                break;

            /*
            case NetDataOpcode.RacketSync:
                RacketSyncData racketSyncData = new RacketSyncData();
                racketSyncData.FromObjArray((object[])photonEvent.CustomData);
                RacketSyncMessage racketSyncMessage = new RacketSyncMessage(racketSyncData.PlayerId, racketSyncData.LocalPosition);

                GameManager.Instance.MessagingSystem.SendMessage(racketSyncMessage);
                break;
                */

            case NetDataOpcode.BallDrop:
                
                BallDropData dropData = new BallDropData();
                dropData.FromObjArray(photonEvent.CustomData);
                
                BallDropMessage initCountdown = new BallDropMessage(dropData.PlayerId == 0 ? true : false);
                GameManager.Instance.MessagingSystem.SendMessage(initCountdown);
                break;
            
            
            case NetDataOpcode.None:
            default:
                Debug.LogWarning("[Photon] Unhandled opcode: " + photonEvent.Code);
                break;    
        }
    }
    
    public override void OnConnectedToMaster()
    {
        Debug.Log("[Photon] Connected.");
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("[Photon] Joined lobby.");
        GameManager.Instance.MessagingSystem.SendMessage(new ConnectedToServerMessage(true));
    }
    
    public override void OnJoinedRoom()
    {
        Debug.Log("[Photon] Joined room.");
        GameManager.Instance.MessagingSystem.SendMessage(new JoinedLobbyMessage(true));
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("[Photon] Create room failed.");
        GameManager.Instance.MessagingSystem.SendMessage(new JoinedLobbyMessage(true, message));
    }
    
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("[Photon] Join room failed.");
        GameManager.Instance.MessagingSystem.SendMessage(new JoinedLobbyMessage(true, message));
    }
}
