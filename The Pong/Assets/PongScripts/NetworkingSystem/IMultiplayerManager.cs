﻿
public interface IMultiplayerManager
{
    void Init();
    void Connect();
    void CreateLobby(string lobbyName);
    void JoinLobby(string lobbyName);
    void Disconnect();
    void SendData(byte opcode, NetDataBase sendData);
}
