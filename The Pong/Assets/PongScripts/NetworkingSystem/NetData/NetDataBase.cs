﻿
using System.Collections.Generic;
using System.IO;
using System.Reflection.Emit;
using System.Runtime.Serialization.Formatters.Binary;
using ExitGames.Client.Photon;
using UnityEngine;

public class NetDataOpcode
{
    public const byte None = 0;
    public const byte PlayerReady = 1;
    public const byte StartGame = 2;
    public const byte PlayerMove = 3;
    public const byte BallSync = 4;
    public const byte RacketSync = 5;
    public const byte BallDrop = 6;
}

public abstract class NetDataBase
{
    public byte Opcode;
    public abstract object ToObjArray();
    public abstract void FromObjArray(object data);
}

public class PlayerReadyData : NetDataBase
{
    public bool IsReady;
    
    public PlayerReadyData()
    {
        Opcode = NetDataOpcode.PlayerReady;
    }
    
    public PlayerReadyData(bool isReady)
    {
        Opcode = NetDataOpcode.PlayerReady;
        
        IsReady = isReady;
    }

    public override object ToObjArray()
    {
        return (object) IsReady;
    }

    public override void FromObjArray(object data)
    {
        IsReady = (bool) data;
    }
}

public class StartGameData : NetDataBase
{
    public StartGameData()
    {
        Opcode = NetDataOpcode.StartGame;
    }

    public override object ToObjArray()
    {
        return null;
    }

    public override void FromObjArray(object data)
    {
    }
}

public class PlayerMoveData : NetDataBase
{
    public ControlButtonType ButtonType;
    public bool IsButtonPressed;

    public PlayerMoveData()
    {
        Opcode = NetDataOpcode.PlayerMove;
    }
    
    public PlayerMoveData(RacketControlMessage controlMessage)
    {
        Opcode = NetDataOpcode.PlayerMove;

        ButtonType = controlMessage.ButtonType;
        IsButtonPressed = controlMessage.IsButtonPressed;
    }
    
    public override object ToObjArray()
    {
        object[] arr = new object[2];
        arr[0] = (object) ButtonType;
        arr[1] = (object) IsButtonPressed;

        return arr;
    }

    public override void FromObjArray(object data)
    {
        var arr = (object[]) data;
        
        ButtonType = (ControlButtonType) arr[0];
        IsButtonPressed = (bool) arr[1];
    }
}

public class BallSyncData : NetDataBase
{
    public Vector3 LocalPosition;
    public Vector3 LocalEuler;

    public BallSyncData()
    {
        Opcode = NetDataOpcode.BallSync;
    }
    
    public BallSyncData(BallSyncMessage syncMessage)
    {
        Opcode = NetDataOpcode.BallSync;

        LocalPosition = syncMessage.LocalPosition;
        LocalEuler = syncMessage.LocalEuler;
    }

    public override object ToObjArray()
    {
        object[] arr = new object[6];
        arr[0] = LocalPosition.x;
        arr[1] = LocalPosition.y;
        arr[2] = LocalPosition.z;
        arr[3] = LocalEuler.x;
        arr[4] = LocalEuler.y;
        arr[5] = LocalEuler.z;

        return arr;
    }

    public override void FromObjArray(object data)
    {
        var arr = (object[]) data;
        
        LocalPosition.x = (float)arr[0];
        LocalPosition.y = (float)arr[1];
        LocalPosition.z = (float)arr[2];
        LocalEuler.x = (float)arr[3];
        LocalEuler.y = (float)arr[4];
        LocalEuler.z = (float)arr[5];
    }
}

public class RacketSyncData : NetDataBase
{
    public int PlayerId;
    public Vector3 LocalPosition;

    public RacketSyncData()
    {
        Opcode = NetDataOpcode.BallSync;
    }
    
    public RacketSyncData(RacketSyncMessage syncMessage)
    {
        Opcode = NetDataOpcode.RacketSync;

        PlayerId = syncMessage.PlayerId;
        LocalPosition = syncMessage.LocalPosition;
    }

    public override object ToObjArray()
    {
        object[] arr = new object[4];
        arr[0] = LocalPosition.x;
        arr[1] = LocalPosition.y;
        arr[2] = LocalPosition.z;
        arr[3] = PlayerId;

        return arr;
    }

    public override void FromObjArray(object data)
    {
        var arr = (object[]) data;
        
        LocalPosition.x = (float)arr[0];
        LocalPosition.y = (float)arr[1];
        LocalPosition.z = (float)arr[2];
        PlayerId = (int) arr[3];
    }
}

public class BallDropData : NetDataBase
{
    public int PlayerId;

    public BallDropData()
    {
        Opcode = NetDataOpcode.BallDrop;
    }
    
    public BallDropData(int playerId)
    {
        Opcode = NetDataOpcode.BallDrop;
        PlayerId = playerId;
    }
    
    public override object ToObjArray()
    {
        return (object) PlayerId;
    }

    public override void FromObjArray(object data)
    {
        PlayerId = (int) data;
    }
}