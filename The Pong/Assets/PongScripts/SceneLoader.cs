﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public bool LoadAtStart;
    public List<string> ScenesToLoad;
    
    private void Start()
    {
        if (LoadAtStart)
        {
            LoadScenes();
        }
    }

    public void LoadScenes()
    {
        foreach (var scene in ScenesToLoad)
        {
            SceneManager.LoadScene(scene, LoadSceneMode.Additive);
        }
    }
    
    public void UnloadScenes()
    {
        foreach (var scene in ScenesToLoad)
        {
            SceneManager.UnloadSceneAsync(scene, UnloadSceneOptions.None);
        }
    }
}
